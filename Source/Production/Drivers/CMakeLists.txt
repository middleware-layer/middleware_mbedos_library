#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_MBedOS_Library                                            #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

coloredMessage(BoldYellow "Loading Drivers CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(DIRECTORY_NAME_SRC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/Drivers/Src")
set(DIRECTORY_NAME_INC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/Drivers/Inc")

#############################################
#   A.1. Set Options                        #
#############################################

set(Drivers_Sources     ${DIRECTORY_NAME_SRC}/AnalogIn.cpp
                        ${DIRECTORY_NAME_SRC}/BusIn.cpp
                        ${DIRECTORY_NAME_SRC}/BusInOut.cpp
                        ${DIRECTORY_NAME_SRC}/BusOut.cpp
                        ${DIRECTORY_NAME_SRC}/CAN.cpp
                        ${DIRECTORY_NAME_SRC}/Ethernet.cpp
                        ${DIRECTORY_NAME_SRC}/FlashIAP.cpp
                        ${DIRECTORY_NAME_SRC}/I2C.cpp
                        ${DIRECTORY_NAME_SRC}/I2CSlave.cpp
                        ${DIRECTORY_NAME_SRC}/InterruptIn.cpp
                        ${DIRECTORY_NAME_SRC}/InterruptManager.cpp
                        ${DIRECTORY_NAME_SRC}/RawSerial.cpp
                        ${DIRECTORY_NAME_SRC}/Serial.cpp
                        ${DIRECTORY_NAME_SRC}/SerialBase.cpp
                        ${DIRECTORY_NAME_SRC}/SPI.cpp
                        ${DIRECTORY_NAME_SRC}/SPISlave.cpp
                        ${DIRECTORY_NAME_SRC}/Ticker.cpp
                        ${DIRECTORY_NAME_SRC}/Timeout.cpp
                        ${DIRECTORY_NAME_SRC}/Timer.cpp
                        ${DIRECTORY_NAME_SRC}/TimerEvent.cpp
                        ${DIRECTORY_NAME_SRC}/UARTSerial.cpp
                        PARENT_SCOPE)

set(Drivers_Headers     PARENT_SCOPE)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################

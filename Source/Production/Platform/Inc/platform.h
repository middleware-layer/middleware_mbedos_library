/* mbed Microcontroller Library
 * Copyright (c) 2006-2013 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <Production/Platform/Inc/mbed_retarget.h>
#include <Production/MbedOS/platform/Inc/mbed_toolchain.h>

#include <Production/MbedOS/target/Inc/device.h>

#ifdef USE_STM32F769I_DISCO
#include <Production/BSP/STM32F769I-Discovery/Inc/PinNames.h>
#include <Production/BSP/STM32F769I-Discovery/Inc/PeripheralNames.h>
#endif

#ifdef STUBS
#include <sys/stat.h>
#endif

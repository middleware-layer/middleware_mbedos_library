/**
 ******************************************************************************
 * @file    mBedOSLibrary_conf.h
 * @author  leonardo.pereira
 * @version v1.0
 * @date    25 de jun de 2017
 * @brief
 ******************************************************************************
 */

#pragma once

#ifdef __cplusplus
extern "C"
{
#endif

   /**
    * @brief Revision ID
    */
#define MBEDOS_LIBRARY_VERSION                               000001

#if defined(MIDDLEWARE_USE_STM32F4)
#include <stm32f4_driverLayer_conf.h>
#elif defined(MIDDLEWARE_USE_STM32F7)
#include <stm32f7_driverLayer_conf.h>
#endif

#include <Production/mbed_config.h>

#include <Production/rtosLibrary_conf.h>

#define _GLIBCXX11_USE_C99_STDIO 1

#define _GLIBCXX11_USE_C99_STDLIB 1

//////////////////////
/// MbedOS configs ///
//////////////////////

#if MBED_CONF_RTOS_PRESENT
// RTOS present, this is valid only for mbed OS 5
#define MBED_MAJOR_VERSION 5
#define MBED_MINOR_VERSION 7
#define MBED_PATCH_VERSION 6
#else
// mbed 2
#define MBED_MAJOR_VERSION 2
#define MBED_MINOR_VERSION 0
#define MBED_PATCH_VERSION MBED_LIBRARY_VERSION
#endif

#define MBED_ENCODE_VERSION(major, minor, patch) ((major)*10000 + (minor)*100 + (patch))
#define MBED_VERSION MBED_ENCODE_VERSION(MBED_MAJOR_VERSION, MBED_MINOR_VERSION, MBED_PATCH_VERSION)
#if MBED_CONF_RTOS_PRESENT
#include "rtos/rtos.h"
#endif

#if MBED_CONF_NSAPI_PRESENT
#include "netsocket/nsapi.h"
#include "netsocket/nsapi_ppp.h"
#endif

#if MBED_CONF_EVENTS_PRESENT
#include "events/mbed_events.h"
#endif

#if MBED_CONF_FILESYSTEM_PRESENT
#include "filesystem/mbed_filesystem.h"
#endif

#include <MbedOS/platform/mbed_toolchain.h>
#include <Production/Platform/platform.h>

// Useful C libraries
#include <math.h>
#include <time.h>

// mbed Debug libraries
#include <MbedOS/platform/mbed_error.h>
#include <MbedOS/platform/mbed_interface.h>
#include <MbedOS/platform/mbed_assert.h>
#include <MbedOS/platform/mbed_debug.h>

/////////////////////////////////////////
/// Little File System Configurations ///
/////////////////////////////////////////

/**
 * @brief   Minimum size of a block read. This determines the size of read buffers.
 *          This may be larger than the physical read size to improve performance by caching more of the block device
 */
#define MBED_LFS_READ_SIZE          64

/**
 * @brief   Minimum size of a block program. This determines the size of program buffers.
 *          This may be larger than the physical program size to improve performance by caching more of the block device
 */
#define MBED_LFS_PROG_SIZE          64

/**
 * @brief   Size of an erasable block. This does not impact ram consumption and may be larger than the physical erase size.
 *          However, this should be kept small as each file currently takes up an entire block
 */
#define MBED_LFS_BLOCK_SIZE         512

/**
 * @brief   Number of blocks to lookahead during block allocation.
 *          A larger lookahead reduces the number of passes required to allocate a block.
 *          The lookahead buffer requires only 1 bit per block so it can be quite large with little ram impact.
 *          Should be a multiple of 32
 */
#define MBED_LFS_LOOKAHEAD          512

/**
 * @brief   Enables info logging, true = enabled, false = disabled, null = disabled only in release builds
 */
#define MBED_LFS_ENABLE_INFO        false

#ifdef __cplusplus
}
#endif

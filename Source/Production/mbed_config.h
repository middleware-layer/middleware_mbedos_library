/*
 * mbed SDK
 * Copyright (c) 2017 ARM Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Automatically generated configuration file.
// DO NOT EDIT, content will be overwritten.
#pragma once

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_ENABLE_PPP_TRACE                   0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_ADDR_TIMEOUT                       5

/**
 * @brief  set by library:events
 */
#define MBED_CONF_EVENTS_SHARED_EVENTSIZE                 256

/**
 * @brief  set by library:events
 */
#define MBED_CONF_EVENTS_SHARED_HIGHPRIO_STACKSIZE        1024

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_IPV4_ENABLED                       0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_DEFAULT_THREAD_STACKSIZE           512

/**
 * @brief  set by library:ppp-cell-iface
 */
#define MBED_CONF_PPP_CELL_IFACE_APN_LOOKUP               0

/**
 * @brief  set by library:events
 */
#define MBED_CONF_EVENTS_PRESENT                          0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_TCPIP_THREAD_STACKSIZE             1200

/**
 * @brief  set by library:lwip
 */
#define NSAPI_PPP_IPV4_AVAILABLE                          0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_PPP_THREAD_STACKSIZE               768

/**
 * @brief  set by library:lwip
 */
#define NSAPI_PPP_IPV6_AVAILABLE                          0

/**
 * @brief  set by library:platform
 */
#define MBED_CONF_PLATFORM_STDIO_FLUSH_AT_EXIT            1

/**
 * @brief  set by library:drivers
 */
#define MBED_CONF_DRIVERS_UART_SERIAL_RXBUF_SIZE          256

/**
 * @brief  set by library:nsapi
 */
#define MBED_CONF_NSAPI_PRESENT                           0

/**
 * @brief  set by library:filesystem
 */
#define MBED_CONF_FILESYSTEM_PRESENT                      0

/**
 * @brief  set by library:ppp-cell-iface
 */
#define MBED_CONF_PPP_CELL_IFACE_BAUD_RATE                115200

/**
 * @brief  set by library:ppp-cell-iface
 */
#define MBED_CONF_PPP_CELL_IFACE_AT_PARSER_BUFFER_SIZE    256

/**
 * @brief  set by library:platform
 */
#define MBED_CONF_PLATFORM_STDIO_BAUD_RATE                9600

/**
 * @brief  set by target:DISCO_F769NI
 */
#define CLOCK_SOURCE                                      USE_PLL_HSE_XTAL|USE_PLL_HSI

/**
 * @brief  set by library:lwip
 */
#define NSAPI_PPP_AVAILABLE                               0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_IPV6_ENABLED                       0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_IP_VER_PREF                        4

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_TCP_SERVER_MAX                     4

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_TCP_SOCKET_MAX                     4

/**
 * @brief  set by library:rtos
 */
#define MBED_CONF_RTOS_PRESENT                            0

/**
 * @brief  set by library:events
 */
#define MBED_CONF_EVENTS_SHARED_DISPATCH_FROM_APPLICATION 0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_TCP_ENABLED                        0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_DEBUG_ENABLED                      0

/**
 * @brief  set by library:ppp-cell-iface
 */
#define MBED_CONF_PPP_CELL_IFACE_AT_PARSER_TIMEOUT        8000

/**
 * @brief  set by library:events
 */
#define MBED_CONF_EVENTS_SHARED_STACKSIZE                 1024

/**
 * @brief  set by library:drivers
 */
#define MBED_CONF_DRIVERS_UART_SERIAL_TXBUF_SIZE          256

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_UDP_SOCKET_MAX                     4

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_USE_MBED_TRACE                     0

/**
 * @brief  set by library:platform
 */
#define MBED_CONF_PLATFORM_STDIO_CONVERT_NEWLINES         0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_ADDR_TIMEOUT_MODE                  1

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_ETHERNET_ENABLED                   0

/**
 * @brief  set by library:lwip
 */
#define MBED_CONF_LWIP_SOCKET_MAX                         4

/**
 * @brief  set by library:events
 */
#define MBED_CONF_EVENTS_SHARED_HIGHPRIO_EVENTSIZE        256

//////////////
/// Macros ///
//////////////

/**
 * @brief  defined by library:utest
 */
#define UNITY_INCLUDE_CONFIG_H

/**
 *
 */
#define TRANSACTION_QUEUE_SIZE_SPI                       2

/**
 ******************************************************************************
 * @file    I2C_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_DRIVERS_INC_I2C_STUB_HPP
#define SOURCE_TESTING_STUBS_DRIVERS_INC_I2C_STUB_HPP

#include <Production/Drivers/Inc/I2C.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#endif // SOURCE_TESTING_STUBS_DRIVERS_INC_I2C_STUB_HPP

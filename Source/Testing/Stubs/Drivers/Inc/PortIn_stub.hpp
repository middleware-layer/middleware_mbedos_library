/**
 ******************************************************************************
 * @file    PortIn_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_DRIVERS_INC_PORTIN_STUB_HPP
#define SOURCE_TESTING_STUBS_DRIVERS_INC_PORTIN_STUB_HPP

#include <Production/Drivers/Inc/PortIn.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#endif // SOURCE_TESTING_STUBS_DRIVERS_INC_PORTIN_STUB_HPP

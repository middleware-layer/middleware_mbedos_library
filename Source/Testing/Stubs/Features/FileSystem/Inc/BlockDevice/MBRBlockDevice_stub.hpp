/**
 ******************************************************************************
 * @file    MBRBlockDevice_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_FEATURES_FILESYSTEM_INC_BLOCKDEVICE_MBRBLOCKDEVICE_STUB_HPP
#define SOURCE_TESTING_STUBS_FEATURES_FILESYSTEM_INC_BLOCKDEVICE_MBRBLOCKDEVICE_STUB_HPP

#include <Production/Features/FileSystem/Inc/BlockDevice/MBRBlockDevice.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#endif // SOURCE_TESTING_STUBS_FEATURES_FILESYSTEM_INC_BLOCKDEVICE_MBRBLOCKDEVICE_STUB_HPP

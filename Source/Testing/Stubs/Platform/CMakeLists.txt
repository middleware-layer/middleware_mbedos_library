#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_MBedOS_Library                                            #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

coloredMessage(BoldYellow "Loading Platform CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(DIRECTORY_NAME_SRC "${PROJECT_STUB_TESTING_SOURCE_FOLDER}/Platform/Src")
set(DIRECTORY_NAME_INC "${PROJECT_STUB_TESTING_SOURCE_FOLDER}/Platform/Inc")

#############################################
#   A.1. Set Options                        #
#############################################

set(Stubs_Platform_Sources    ${DIRECTORY_NAME_SRC}/CallChain_stub.cpp
                              ${DIRECTORY_NAME_SRC}/FileBase_stub.cpp
                              ${DIRECTORY_NAME_SRC}/FileHandle_stub.cpp
                              ${DIRECTORY_NAME_SRC}/FilePath_stub.cpp
                              ${DIRECTORY_NAME_SRC}/FileSystemHandle_stub.cpp
                              ${DIRECTORY_NAME_SRC}/mbed_poll_stub.cpp
                              ${DIRECTORY_NAME_SRC}/mbed_retarget_stub.cpp
                              ${DIRECTORY_NAME_SRC}/mbed_rtc_time_stub.cpp
                              ${DIRECTORY_NAME_SRC}/mbed_wait_api_rtos_stub.cpp
                              ${DIRECTORY_NAME_SRC}/Stream_stub.cpp
                              PARENT_SCOPE)

set(Stubs_Platform_Headers    PARENT_SCOPE)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################

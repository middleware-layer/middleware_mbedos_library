/**
 ******************************************************************************
 * @file    CallChain_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_CALLCHAIN_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_CALLCHAIN_STUB_HPP

#include <Production/Platform/Inc/CallChain.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class CallChain_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_CallChain
   {
      public:
         virtual ~I_CallChain();
   };

   class _Mock_CallChain : public I_CallChain
   {
      public:
         _Mock_CallChain();
   };

   typedef ::testing::NiceMock<_Mock_CallChain> Mock_CallChain;

   void stub_setImpl(I_CallChain* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_CALLCHAIN_STUB_HPP

/**
 ******************************************************************************
 * @file    FileBase_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_BASE_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_BASE_STUB_HPP

#include <Production/Platform/Inc/FileBase.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class FileBase_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_FileBase
   {
      public:
         virtual ~I_FileBase();
   };

   class _Mock_FileBase : public I_FileBase
   {
      public:
         _Mock_FileBase();
   };

   typedef ::testing::NiceMock<_Mock_FileBase> Mock_FileBase;

   void stub_setImpl(I_FileBase* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_BASE_STUB_HPP

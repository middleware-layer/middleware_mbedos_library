/**
 ******************************************************************************
 * @file    FileHandle_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_HANDLE_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_HANDLE_STUB_HPP

#include <Production/Platform/Inc/FileHandle.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class FileHandle_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_FileHandle
   {
      public:
         virtual ~I_FileHandle();   };

   class _Mock_FileHandle : public I_FileHandle
   {
      public:
         _Mock_FileHandle();
   };

   typedef ::testing::NiceMock<_Mock_FileHandle> Mock_FileHandle;

   void stub_setImpl(I_FileHandle* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_HANDLE_STUB_HPP

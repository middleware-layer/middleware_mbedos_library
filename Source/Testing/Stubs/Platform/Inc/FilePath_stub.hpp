/**
 ******************************************************************************
 * @file    FilePath_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_PATH_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_PATH_STUB_HPP

#include <Production/Platform/Inc/FilePath.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class FilePath_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_FilePath
   {
      public:
         virtual ~I_FilePath();
   };

   class _Mock_FilePath : public I_FilePath
   {
      public:
         _Mock_FilePath();
   };

   typedef ::testing::NiceMock<_Mock_FilePath> Mock_FilePath;

   void stub_setImpl(I_FilePath* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_TRANSACTION_STUB_HPP

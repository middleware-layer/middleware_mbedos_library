/**
 ******************************************************************************
 * @file    FileSystemHandle_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_SYSTEM_HANDLE_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_SYSTEM_HANDLE_STUB_HPP

#include <Production/Platform/Inc/FileSystemHandle.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class FileSystemHandle_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_FileSystemHandle
   {
      public:
         virtual ~I_FileSystemHandle();
   };

   class _Mock_FileSystemHandle : public I_FileSystemHandle
   {
      public:
         _Mock_FileSystemHandle();
   };

   typedef ::testing::NiceMock<_Mock_FileSystemHandle> Mock_FileSystemHandle;

   void stub_setImpl(I_FileSystemHandle* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_FILE_SYSTEM_HANDLE_STUB_HPP

/**
 ******************************************************************************
 * @file    Stream_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_STREAM_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_STREAM_STUB_HPP

#include <Production/Platform/Inc/Stream.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class Stream_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_Stream
   {
      public:
         virtual ~I_Stream();
   };

   class _Mock_Stream : public I_Stream
   {
      public:
         _Mock_Stream();
   };

   typedef ::testing::NiceMock<_Mock_Stream> Mock_Stream;

   void stub_setImpl(I_Stream* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_STREAM_STUB_HPP

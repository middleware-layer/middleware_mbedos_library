/**
 ******************************************************************************
 * @file    MBed_Poll_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_POLL_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_POLL_STUB_HPP

#include <Production/Platform/Inc/mbed_poll.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class MBed_Poll_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_MBed_Poll
   {
      public:
         virtual ~I_MBed_Poll();
   };

   class _Mock_MBed_Poll : public I_MBed_Poll
   {
      public:
         _Mock_MBed_Poll();
   };

   typedef ::testing::NiceMock<_Mock_MBed_Poll> Mock_MBed_Poll;

   void stub_setImpl(I_MBed_Poll* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_POLL_STUB_HPP

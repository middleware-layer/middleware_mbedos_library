/**
 ******************************************************************************
 * @file    MBed_Retarget_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_RETARGET_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_RETARGET_STUB_HPP

#include <Production/Platform/Inc/mbed_retarget.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class MBed_Retarget_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_MBed_Retarget
   {
      public:
         virtual ~I_MBed_Retarget();
   };

   class _Mock_MBed_Retarget : public I_MBed_Retarget
   {
      public:
         _Mock_MBed_Retarget();
   };

   typedef ::testing::NiceMock<_Mock_MBed_Retarget> Mock_MBed_Retarget;

   void stub_setImpl(I_MBed_Retarget* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_RETARGET_STUB_HPP

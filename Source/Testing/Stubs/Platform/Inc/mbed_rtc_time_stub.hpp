/**
 ******************************************************************************
 * @file    mbed_rtc_Time_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_RTC_TIME_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_RTC_TIME_STUB_HPP

#include <Production/Platform/Inc/mbed_rtc_time.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class MBed_RTC_Time_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_MBed_RTC_Time
   {
      public:
         virtual ~I_MBed_RTC_Time();
   };

   class _Mock_MBed_RTC_Time : public I_MBed_RTC_Time
   {
      public:
         _Mock_MBed_RTC_Time();
   };

   typedef ::testing::NiceMock<_Mock_MBed_RTC_Time> Mock_MBed_RTC_Time;

   void stub_setImpl(I_MBed_RTC_Time* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_RTC_TIME_STUB_HPP

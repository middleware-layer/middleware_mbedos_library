/**
 ******************************************************************************
 * @file    MBed_Wait_API_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_WAIT_API_STUB_HPP
#define SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_WAIT_API_STUB_HPP

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace mbed
{
   class MBed_Wait_API_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_MBed_Wait_API
   {
      public:
         virtual ~I_MBed_Wait_API();
   };

   class _Mock_MBed_Wait_API : public I_MBed_Wait_API
   {
      public:
         _Mock_MBed_Wait_API();
   };

   typedef ::testing::NiceMock<_Mock_MBed_Wait_API> Mock_MBed_Wait_API;

   void stub_setImpl(I_MBed_Wait_API* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_PLATFORM_INC_MBED_WAIT_API_STUB_HPP

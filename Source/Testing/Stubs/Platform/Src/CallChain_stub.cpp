/**
 ******************************************************************************
 * @file    CallChain_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/CallChain_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_CallChain* p_CallChain_Impl = NULL;

   const char* CallChain_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle CallChain functions";
   }

   void stub_setImpl(I_CallChain* pNewImpl)
   {
      p_CallChain_Impl = pNewImpl;
   }

   static I_CallChain* CallChain_Stub_Get_Impl(void)
   {
      if(p_CallChain_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to CallChain functions. Did you forget"
                   << "to call Stubs::mbed::stub_setImpl() ?" << std::endl;

         throw CallChain_StubImplNotSetException();
      }

      return p_CallChain_Impl;
   }

      ////////////////////////////////////
      // I_CallChain Methods Definition //
      ////////////////////////////////////

   I_CallChain::~I_CallChain()
   {
      if(p_CallChain_Impl == this)
      {
         p_CallChain_Impl = NULL;
      }
   }

   ////////////////////////////////////////
   // _Mock_CallChain Methods Definition //
   ////////////////////////////////////////

   _Mock_CallChain::_Mock_CallChain()
   {

   }

       //////////////////////////////////
       // Definition of Non Stub Class //
       //////////////////////////////////

   CallChain::CallChain(int size)
   {
   }

   CallChain::~CallChain()
   {
   }
}


/**
 ******************************************************************************
 * @file    FileBase_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/FileBase_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_FileBase* p_FileBase_Impl = NULL;

   const char* FileBase_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle FileBase functions";
   }

   void stub_setImpl(I_FileBase* pNewImpl)
   {
      p_FileBase_Impl = pNewImpl;
   }

   static I_FileBase* FileBase_Stub_Get_Impl(void)
   {
      if(p_FileBase_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to FileBase functions. Did you forget"
                   << "to call Stubs::mbed::stub_setImpl() ?" << std::endl;

         throw FileBase_StubImplNotSetException();
      }

      return p_FileBase_Impl;
   }

      ///////////////////////////////////
      // I_FileBase Methods Definition //
      ///////////////////////////////////

   I_FileBase::~I_FileBase()
   {
      if(p_FileBase_Impl == this)
      {
         p_FileBase_Impl = NULL;
      }
   }

   ///////////////////////////////////////
   // _Mock_FileBase Methods Definition //
   ///////////////////////////////////////

   _Mock_FileBase::_Mock_FileBase()
   {

   }

       //////////////////////////////////
       // Definition of Non Stub Class //
       //////////////////////////////////

   FileBase::FileBase(const char *name,
                      PathType t) :
            _next(NULL),
            _name(name),
            _path_type(t)
   {
   }

   FileBase::~FileBase()
   {
   }
}


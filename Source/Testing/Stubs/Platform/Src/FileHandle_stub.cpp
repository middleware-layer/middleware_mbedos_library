/**
 ******************************************************************************
 * @file    FileHandle_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/FileHandle_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_FileHandle* p_FileHandle_Impl = NULL;

   const char* FileHandle_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle FileHandle functions";
   }

   void stub_setImpl(I_FileHandle* pNewImpl)
   {
      p_FileHandle_Impl = pNewImpl;
   }

   static I_FileHandle* FileHandle_Stub_Get_Impl(void)
   {
      if(p_FileHandle_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to FileHandle functions. Did you forget"
                   << "to call Stubs::mbed::stub_setImpl() ?" << std::endl;

         throw FileHandle_StubImplNotSetException();
      }

      return p_FileHandle_Impl;
   }

      /////////////////////////////////////
      // I_FileHandle Methods Definition //
      /////////////////////////////////////

   I_FileHandle::~I_FileHandle()
   {
      if(p_FileHandle_Impl == this)
      {
         p_FileHandle_Impl = NULL;
      }
   }

   /////////////////////////////////////////
   // _Mock_FileHandle Methods Definition //
   /////////////////////////////////////////

   _Mock_FileHandle::_Mock_FileHandle()
   {

   }

       //////////////////////////////////
       // Definition of Non Stub Class //
       //////////////////////////////////
}


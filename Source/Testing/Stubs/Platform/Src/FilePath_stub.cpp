/**
 ******************************************************************************
 * @file    FilePath_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/FilePath_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_FilePath* p_FilePath_Impl = NULL;

   const char* FilePath_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle FilePath functions";
   }

   void stub_setImpl(I_FilePath* pNewImpl)
   {
      p_FilePath_Impl = pNewImpl;
   }

   static I_FilePath* FilePath_Stub_Get_Impl(void)
   {
      if(p_FilePath_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to FilePath functions. Did you forget"
                   << "to call Stubs::mbed::stub_setImpl() ?" << std::endl;

         throw FilePath_StubImplNotSetException();
      }

      return p_FilePath_Impl;
   }

      ///////////////////////////////////
      // I_FilePath Methods Definition //
      ///////////////////////////////////

   I_FilePath::~I_FilePath()
   {
      if(p_FilePath_Impl == this)
      {
         p_FilePath_Impl = NULL;
      }
   }

   ///////////////////////////////////////
   // _Mock_FilePath Methods Definition //
   ///////////////////////////////////////

   _Mock_FilePath::_Mock_FilePath()
   {

   }

       //////////////////////////////////
       // Definition of Non Stub Class //
       //////////////////////////////////

   FilePath::FilePath(const char* file_path)
   {
   }
}


/**
 ******************************************************************************
 * @file    FileSystemHandle_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/FileSystemHandle_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_FileSystemHandle* p_FileSystemHandle_Impl = NULL;

   const char* FileSystemHandle_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle FileSystemHandle functions";
   }

   void stub_setImpl(I_FileSystemHandle* pNewImpl)
   {
      p_FileSystemHandle_Impl = pNewImpl;
   }

   static I_FileSystemHandle* FileSystemHandle_Stub_Get_Impl(void)
   {
      if(p_FileSystemHandle_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to FileSystemHandle functions. Did you forget"
                   << "to call Stubs::mbed::stub_setImpl() ?" << std::endl;

         throw FileSystemHandle_StubImplNotSetException();
      }

      return p_FileSystemHandle_Impl;
   }

      ///////////////////////////////////////////
      // I_FileSystemHandle Methods Definition //
      ///////////////////////////////////////////

   I_FileSystemHandle::~I_FileSystemHandle()
   {
      if(p_FileSystemHandle_Impl == this)
      {
         p_FileSystemHandle_Impl = NULL;
      }
   }

   ///////////////////////////////////////////////
   // _Mock_FileSystemHandle Methods Definition //
   ///////////////////////////////////////////////

   _Mock_FileSystemHandle::_Mock_FileSystemHandle()
   {

   }

       //////////////////////////////////
       // Definition of Non Stub Class //
       //////////////////////////////////
}


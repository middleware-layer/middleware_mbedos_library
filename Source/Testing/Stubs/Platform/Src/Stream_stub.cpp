/**
 ******************************************************************************
 * @file    Stream_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/Stream_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_Stream* p_Stream_Impl = NULL;

   const char* Stream_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle Stream functions";
   }

   void stub_setImpl(I_Stream* pNewImpl)
   {
      p_Stream_Impl = pNewImpl;
   }

   static I_Stream* Stream_Stub_Get_Impl(void)
   {
      if(p_Stream_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to Stream functions. Did you forget"
                   << "to call Stubs::mbed::stub_setImpl() ?" << std::endl;

         throw Stream_StubImplNotSetException();
      }

      return p_Stream_Impl;
   }

      /////////////////////////////////
      // I_Stream Methods Definition //
      /////////////////////////////////

   I_Stream::~I_Stream()
   {
      if(p_Stream_Impl == this)
      {
         p_Stream_Impl = NULL;
      }
   }

   ///////////////////////////////////
   // _Mock_Stream Methods Definition //
   ///////////////////////////////////

   _Mock_Stream::_Mock_Stream()
   {
   }

               //////////////////////////////////
               // Definition of Non Stub Class //
               //////////////////////////////////

   Stream::Stream(const char *name)
   {
   }

   Stream::~Stream()
   {
   }
}


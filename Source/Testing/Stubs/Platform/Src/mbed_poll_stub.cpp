/**
 ******************************************************************************
 * @file    MBed_Poll_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/mbed_poll_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_MBed_Poll* p_MBed_Poll_Impl = NULL;

   const char* MBed_Poll_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle MBed_Poll functions";
   }

   void stub_setImpl(I_MBed_Poll* pNewImpl)
   {
      p_MBed_Poll_Impl = pNewImpl;
   }

   static I_MBed_Poll* MBed_Poll_Stub_Get_Impl(void)
   {
      if(p_MBed_Poll_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to MBed_Poll functions. Did you forget"
                   << "to call Stubs::USB::stub_setImpl() ?" << std::endl;

         throw MBed_Poll_StubImplNotSetException();
      }

      return p_MBed_Poll_Impl;
   }

      ////////////////////////////////////
      // I_MBed_Poll Methods Definition //
      ////////////////////////////////////

   I_MBed_Poll::~I_MBed_Poll()
   {
      if(p_MBed_Poll_Impl == this)
      {
         p_MBed_Poll_Impl = NULL;
      }
   }

   ////////////////////////////////////////
   // _Mock_MBed_Poll Methods Definition //
   ////////////////////////////////////////

   _Mock_MBed_Poll::_Mock_MBed_Poll()
   {

   }

       //////////////////////////////////
       // Definition of Non Stub Class //
       //////////////////////////////////

   int poll(pollfh fhs[],
            unsigned nfhs,
            int timeout)
   {

   }
}


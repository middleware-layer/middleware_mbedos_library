/**
 ******************************************************************************
 * @file    MBed_Retarget_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/mbed_retarget_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_MBed_Retarget* p_MBed_Retarget_Impl = NULL;

   const char* MBed_Retarget_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle MBed_Retarget functions";
   }

   void stub_setImpl(I_MBed_Retarget* pNewImpl)
   {
      p_MBed_Retarget_Impl = pNewImpl;
   }

   static I_MBed_Retarget* MBed_Retarget_Stub_Get_Impl(void)
   {
      if(p_MBed_Retarget_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to MBed_Retarget functions. Did you forget"
                   << "to call Stubs::mbed::stub_setImpl() ?" << std::endl;

         throw MBed_Retarget_StubImplNotSetException();
      }

      return p_MBed_Retarget_Impl;
   }

      ////////////////////////////////////////
      // I_MBed_Retarget Methods Definition //
      ////////////////////////////////////////

   I_MBed_Retarget::~I_MBed_Retarget()
   {
      if(p_MBed_Retarget_Impl == this)
      {
         p_MBed_Retarget_Impl = NULL;
      }
   }

   ////////////////////////////////////////////
   // _Mock_MBed_Retarget Methods Definition //
   ////////////////////////////////////////////

   _Mock_MBed_Retarget::_Mock_MBed_Retarget()
   {

   }
}

//////////////////////////////////
// Definition of Non Stub Class //
//////////////////////////////////

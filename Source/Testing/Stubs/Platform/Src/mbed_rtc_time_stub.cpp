/**
 ******************************************************************************
 * @file    MBed_RTC_Time_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/mbed_rtc_time_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_MBed_RTC_Time* p_MBed_RTC_Time_Impl = NULL;

   const char* MBed_RTC_Time_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle MBed_RTC_Time functions";
   }

   void stub_setImpl(I_MBed_RTC_Time* pNewImpl)
   {
      p_MBed_RTC_Time_Impl = pNewImpl;
   }

   static I_MBed_RTC_Time* MBed_RTC_Time_Stub_Get_Impl(void)
   {
      if(p_MBed_RTC_Time_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to MBed_RTC_Time functions. Did you forget"
                   << "to call Stubs::USB::stub_setImpl() ?" << std::endl;

         throw MBed_RTC_Time_StubImplNotSetException();
      }

      return p_MBed_RTC_Time_Impl;
   }

      ////////////////////////////////////////
      // I_MBed_RTC_Time Methods Definition //
      ////////////////////////////////////////

   I_MBed_RTC_Time::~I_MBed_RTC_Time()
   {
      if(p_MBed_RTC_Time_Impl == this)
      {
         p_MBed_RTC_Time_Impl = NULL;
      }
   }

   ////////////////////////////////////////////
   // _Mock_MBed_RTC_Time Methods Definition //
   ////////////////////////////////////////////

   _Mock_MBed_RTC_Time::_Mock_MBed_RTC_Time()
   {

   }
}

//////////////////////////////////
// Definition of Non Stub Class //
//////////////////////////////////

#include <Production/Platform/Inc/mbed_rtc_time.h>

void set_time(time_t t)
{

}

void attach_rtc(time_t (*read_rtc)(void),
                void (*write_rtc)(time_t),
                void (*init_rtc)(void),
                int (*isenabled_rtc)(void))
{

}

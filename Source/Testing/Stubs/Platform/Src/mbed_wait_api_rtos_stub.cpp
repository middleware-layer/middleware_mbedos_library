/**
 ******************************************************************************
 * @file    MBed_Wait_API_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    22 de apr de 2019
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/Platform/Inc/mbed_wait_api_rtos_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace mbed
{
   static I_MBed_Wait_API* p_MBed_Wait_API_Impl = NULL;

   const char* MBed_Wait_API_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle MBed_Wait_API functions";
   }

   void stub_setImpl(I_MBed_Wait_API* pNewImpl)
   {
      p_MBed_Wait_API_Impl = pNewImpl;
   }

   static I_MBed_Wait_API* MBed_Wait_API_Stub_Get_Impl(void)
   {
      if(p_MBed_Wait_API_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to MBed_Wait_API functions. Did you forget"
                   << "to call Stubs::USB::stub_setImpl() ?" << std::endl;

         throw MBed_Wait_API_StubImplNotSetException();
      }

      return p_MBed_Wait_API_Impl;
   }

      ////////////////////////////////////////
      // I_MBed_Wait_API Methods Definition //
      ////////////////////////////////////////

   I_MBed_Wait_API::~I_MBed_Wait_API()
   {
      if(p_MBed_Wait_API_Impl == this)
      {
         p_MBed_Wait_API_Impl = NULL;
      }
   }

   ////////////////////////////////////////////
   // _Mock_MBed_Wait_API Methods Definition //
   ////////////////////////////////////////////

   _Mock_MBed_Wait_API::_Mock_MBed_Wait_API()
   {

   }
}

//////////////////////////////////
// Definition of Non Stub Class //
//////////////////////////////////

#if MBED_CONF_RTOS_PRESENT
#include <MbedOS/hal/us_ticker_api.h>
#include <MbedOS/platform/mbed_wait_api.h>
#include <MbedOS/platform/mbed_critical.h>
#include <MbedOS/platform/mbed_sleep.h>

#include "rtos/rtos.h"

void wait(float s)
{

}

void wait_ms(int ms)
{

}

void wait_us(int us)
{

}
#endif // #if MBED_CONF_RTOS_PRESENT
